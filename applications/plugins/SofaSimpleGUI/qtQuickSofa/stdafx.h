#ifndef STDAFX_H
#define STDAFX_H

#include "SofaGL.h"
#include "SofaScene.h"

#include <iostream>
#include <QVector>
#include <QVector4D>
#include <QTime>
#include <QObject>
#include <QQuickWindow>
#include <QQuickItem>
#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLShaderProgram>
#include <QQmlApplicationEngine>
#include <QOpenGLContext>
#include <QImage>


#endif // STDAFX_H